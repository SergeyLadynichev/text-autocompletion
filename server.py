# -*- coding: utf-8 -*-

import asyncio
import argparse
import logging

from alphabet import Alphabet


log = logging.getLogger(__name__)
clients = {}


class Server(object):

    def __init__(self):
        self.alphabet = Alphabet()

    def init_dictionary(self, dictionary_path):
        with open(dictionary_path, 'r') as dict_file:
            for line in dict_file:
                self.alphabet.set(line)

        log.info("alphabet init complete")

    def accept_client(self, client_reader, client_writer):
        task = asyncio.Task(self.handle_client(client_reader, client_writer))
        clients[task] = (client_reader, client_writer)

        def client_done(task):
            del clients[task]
            client_writer.close()
            log.info("End Connection")

        log.info("New Connection")
        task.add_done_callback(client_done)

    @asyncio.coroutine
    def handle_client(self, client_reader, client_writer):
        client_writer.write("CYN\n".encode())
        data = yield from asyncio.wait_for(client_reader.readline(), timeout=10.0)

        if data is None:
            return

        sdata = data.decode().rstrip()
        if sdata != "CYN-ACK":
            return

        client_writer.write("ACK\n".encode())
        while True:
            data = yield from asyncio.wait_for(client_reader.readline(), timeout=10.0)

            if data is None:
                return

            sdata = data.decode().rstrip()
            if sdata.upper() == 'ACK':
                client_writer.write("ACK\n".encode())
                break

            response = ("{0}\n".format(self.alphabet.get(sdata)))
            client_writer.write(response.encode())


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dictionary_path', type=str, help='path to the dictionary')
    parser.add_argument('--port', type=int, help='server port')

    args = parser.parse_args()
    if not args.port or not args.dictionary_path:
        log.info("wrong params")
        return

    server = Server()
    server.init_dictionary(dictionary_path=args.dictionary_path)

    loop = asyncio.get_event_loop()
    f = asyncio.start_server(server.accept_client, host=None, port=args.port)
    loop.run_until_complete(f)
    loop.run_forever()

if __name__ == '__main__':
    log = logging.getLogger("")
    formatter = logging.Formatter("%(asctime)s %(levelname)s " + "[%(module)s:%(lineno)d] %(message)s")
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    log.addHandler(ch)
    main()
