# text autocompletion

Сервис автодополнения текста на основе словаря и веса  

alphabet.py - базовая библиотека  
server.py - asyncio сервер  
client.py - клиент для вышеуказанного сервера  

##### Запуск
python3 server.py --port=<номер порта> --dictionary_path=<путь к словарю>  
python3 client.py --port=<номер порта> --host=<хост>


