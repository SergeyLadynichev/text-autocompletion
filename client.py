# -*- coding: utf-8 -*-

import argparse
import asyncio


class Client(object):

    def __init__(self):
        self.clients = {}

    def make_connection(self, host, port, prefix):
        task = asyncio.Task(self.handle_client(host, port, prefix))
        self.clients[task] = (host, port)

        def client_done(task):
            del self.clients[task]
            if len(self.clients) == 0:
                loop = asyncio.get_event_loop()
                loop.stop()

        task.add_done_callback(client_done)

    @asyncio.coroutine
    def handle_client(self, host, port, prefix):
        client_reader, client_writer = yield from asyncio.open_connection(host, port)
        try:
            data = yield from asyncio.wait_for(client_reader.readline(), timeout=10.0)

            if data is None:
                return

            sdata = data.decode().rstrip().upper()
            if sdata != "CYN":
                return

            client_writer.write("CYN-ACK\n".encode())
            data = yield from asyncio.wait_for(client_reader.readline(), timeout=10.0)

            if data is None:
                return

            sdata = data.decode().rstrip().upper()
            if sdata != "ACK":
                return

            client_writer.write(("{0}\n".format(prefix)).encode())
            data = yield from asyncio.wait_for(client_reader.readline(), timeout=10.0)

            if data is None:
                return

            sdata = data.decode().rstrip()
            print(' ')
            for chunk in sdata.split(','):
                print(chunk)
            print(' ')

            client_writer.write("ACK\n".encode())
            yield from asyncio.wait_for(client_reader.readline(), timeout=10.0)

        finally:
            client_writer.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host',  type=str, help='server address')
    parser.add_argument('--port', type=int, help='server port')

    args = parser.parse_args()

    loop = asyncio.get_event_loop()
    client = Client()

    condition = True
    while condition:
        print("Enter command get <prefix>")
        command = input()

        if command == 'exit':
            condition = False
            continue

        action, prefix = command.split(' ')
        if action != 'get' or not prefix:
            condition = False
            print('wrong command')
            continue

        client.make_connection(host=args.host, port=args.port, prefix=prefix)
        loop.run_forever()

if __name__ == '__main__':
    main()
