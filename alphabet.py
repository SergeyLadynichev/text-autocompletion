# -*- coding: utf-8 -*-

from collections import namedtuple


class Node(object):

    NOT_FOUND_MESSAGE = "No results fount"

    @staticmethod
    def _check_node_list(current_step):
        if 'node' not in current_step:
            current_step['node'] = list()

    def _set_end_node(self, current_step, word):
        self._check_node_list(current_step)
        current_step['node'].append(word)

    def _get_end_node(self, current_step, word=None):
        self._check_node_list(current_step)
        return word if word and word in current_step['node'] else current_step['node']

    @staticmethod
    def get_cases(current_step, step=None):
        cases, steps = list(), list()

        if step:
            steps.append(current_step[step])

        if 'node' in current_step:
            cases.extend(current_step['node'])

        while steps:
            step = steps[0]
            keys = step.keys()
            for key in keys:
                if type(step[key]) == dict:
                    steps.append(step[key])
                    if 'node' in step[key]:
                        cases.extend(step[key]['node'])
                    if 'node' in step:
                        cases.extend(step['node'])
            del steps[0]

        cases = set(cases)

        return sorted(cases, key=lambda word: (word.weight, word.content), reverse=True)[:10]\
            if cases else Node.NOT_FOUND_MESSAGE


class Alphabet(Node):

    def __init__(self,):
        self.alphabet_store = {}
        self.Word_tuple = namedtuple('Word', ('content', 'weight'))

    @staticmethod
    def __format_output(words):
        return ','.join(word.content for word in words)

    def get(self, word):
        current_step = self.alphabet_store
        step = str()
        for char in word[::1]:
            step += char

            if step in current_step and step != word:
                current_step = current_step[step]
                continue

            elif step == word and step in current_step:
                cases = list()
                cases.extend(self._get_end_node(current_step, word))
                cases = self.get_cases(current_step, step=step)
                return self.__format_output(cases)

            elif step == word:
                success = self._get_end_node(current_step, word)
                if success:
                    return self.__format_output(success)
                else:
                    current_step = current_step[step]
                    cases = self.get_cases(current_step)
                    return self.__format_output(cases)

            else:
                return self.NOT_FOUND_MESSAGE

    def set(self, payload):
        content, weight = payload.split(' ')
        word = self.Word_tuple._make([content, int(weight)])
        current_step = self.alphabet_store
        step = str()

        for char in word.content[::1]:
            step += char

            if step in current_step:
                current_step = current_step[step]
                continue
            elif step == word.content:
                self._set_end_node(current_step, word)
            else:
                current_step[step] = {}
                current_step = current_step[step]


def main():
    txt_words_count = int(input())
    alphabet = Alphabet()
    user_input = []

    for _ in range(txt_words_count):
        alphabet.set(input())

    user_input_count = int(input())
    for _ in range(user_input_count):
        user_input.append(input())

    print(' ')
    for word in user_input:
        print('\n'.join(alphabet.get(word).split(',')) + '\n')

if __name__ == '__main__':
    main()
